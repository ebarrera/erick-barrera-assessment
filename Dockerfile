FROM maven:3.6.1-jdk-11-slim

WORKDIR /app

# Prepare by downloading dependencies
ADD pom.xml /app/pom.xml
RUN mvn dependency:resolve

# Adding source, compile and package into a fat jar
# This assumes you've configured such a goal in pom.xml
ADD src /app/src
RUN mvn package

ADD scripts/run.sh /app/run.sh

EXPOSE 5008

ENTRYPOINT /app/run.sh