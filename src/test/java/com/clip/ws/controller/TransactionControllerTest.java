package com.clip.ws.controller;

import com.clip.ws.entity.Transaction;
import com.clip.ws.mock.TransactionMock;
import com.clip.ws.representation.request.TransactionRequest;
import com.clip.ws.service.TransactionService;
import com.clip.ws.util.JsonUtil;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.clip.ws.controller.contants.ServiceConstants.USER_HEADER;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(TransactionController.class)
public class TransactionControllerTest extends BaseControllerTest {

    @MockBean
    private TransactionService transactionService;

    @Test
    public void shouldCreateATransaction() throws Exception {
        Transaction transaction = TransactionMock.transaction();
        given(transactionService.create(any(Long.class), any(TransactionRequest.class))).willReturn(transaction);

        TransactionRequest request = TransactionRequest.builder()
                .amount(transaction.getAmount())
                .description(transaction.getDescription())
                .localDate(transaction.getDate())
                .build();

        mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .header(USER_HEADER, transaction.getUserId())
                .content(JsonUtil.toJson(request)))
                .andExpect(status().isCreated())
                .andExpect(content().json("{}"));
    }

    @Test
    public void shouldReturnATransaction() throws Exception {
        Transaction transaction = TransactionMock.transaction();
        given(transactionService.getTransaction(any(UUID.class), any(Long.class))).willReturn(of(transaction));

        mvc.perform(get(String.format("/transactions/%s", transaction.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .header(USER_HEADER, transaction.getUserId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transaction_id", is(transaction.getId().toString())));
    }

    @Test
    public void shouldReturnNotFoundGettingATransaction() throws Exception {
        Transaction transaction = TransactionMock.transaction();
        given(transactionService.getTransaction(any(UUID.class), any(Long.class))).willReturn(empty());

        mvc.perform(get(String.format("/transactions/%s", UUID.randomUUID()))
                .contentType(MediaType.APPLICATION_JSON)
                .header(USER_HEADER, transaction.getUserId()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnAListOfTransactions() throws Exception {
        Transaction transaction = TransactionMock.transaction();
        List<Transaction> transactions = Collections.singletonList(transaction);
        given(transactionService.getTransactions(any(Long.class))).willReturn(transactions);

        mvc.perform(get("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .header(USER_HEADER, transaction.getUserId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(transactions.size())));
    }

    @Test
    public void shouldReturnAnEmptyListOfTransactions() throws Exception {
        List<Transaction> transactions = Collections.emptyList();
        given(transactionService.getTransactions(any(Long.class))).willReturn(transactions);

        mvc.perform(get("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .header(USER_HEADER, 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", IsEmptyCollection.empty()));
    }

}
