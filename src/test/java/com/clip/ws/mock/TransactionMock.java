package com.clip.ws.mock;

import com.clip.ws.entity.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

public class TransactionMock {

    public static Transaction transaction() {
        return Transaction.builder()
                .date(LocalDate.now())
                .userId(1L)
                .description("Taquitos de suadero")
                .amount(BigDecimal.TEN)
                .id(UUID.randomUUID())
                .build();
    }

}
