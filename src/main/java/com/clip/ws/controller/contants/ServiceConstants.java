package com.clip.ws.controller.contants;

public final class ServiceConstants {

    private ServiceConstants() {
    }

    public static final String USER_HEADER = "x-user-id";

}
