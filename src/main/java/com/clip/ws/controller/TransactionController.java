package com.clip.ws.controller;

import com.clip.ws.domain.TransactionMapper;
import com.clip.ws.entity.Transaction;
import com.clip.ws.exception.ResourceNotFoundException;
import com.clip.ws.representation.SumRepresentation;
import com.clip.ws.representation.TransactionRepresentation;
import com.clip.ws.representation.request.TransactionRequest;
import com.clip.ws.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.clip.ws.controller.contants.ServiceConstants.USER_HEADER;
import static com.clip.ws.domain.TransactionMapper.toRepresentation;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api("API related to transactions")
@RestController
@RequestMapping(value = "/transactions", produces = APPLICATION_JSON_VALUE)
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @ApiOperation(value = "Create a new transaction", produces = APPLICATION_JSON_VALUE)
    @PostMapping
    @ResponseStatus(CREATED)
    public TransactionRepresentation createTransaction(@RequestHeader(USER_HEADER) Long userId, @Valid @RequestBody TransactionRequest transactionRequest) {
        Transaction transaction = transactionService.create(userId, transactionRequest);
        return toRepresentation(transaction);
    }

    @ApiOperation(value = "Get all transactions by user", produces = APPLICATION_JSON_VALUE)
    @GetMapping
    public List<TransactionRepresentation> getTransactions(@RequestHeader(USER_HEADER) Long userId) {
        return transactionService.getTransactions(userId).stream()
                .map(TransactionMapper::toRepresentation)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Get a transaction", produces = APPLICATION_JSON_VALUE)
    @GetMapping("/{id}")
    public TransactionRepresentation getTransaction(@PathVariable UUID id, @RequestHeader(USER_HEADER) Long userId) {
        Transaction transaction = transactionService.getTransaction(id, userId).orElseThrow(ResourceNotFoundException::new);
        return toRepresentation(transaction);
    }

    @ApiOperation(value = "Sum all transactions by user", produces = APPLICATION_JSON_VALUE)
    @GetMapping("/sum")
    public SumRepresentation getSum(@RequestHeader(USER_HEADER) Long userId) {
        return SumRepresentation.builder()
                .userId(userId)
                .sum(transactionService.sum(userId))
                .build();
    }

    @ApiOperation(value = "Get a random transaction", produces = APPLICATION_JSON_VALUE)
    @GetMapping("/random")
    public List<TransactionRepresentation> getRandomTransactions() {
        return transactionService.random().stream()
                .map(TransactionMapper::toRepresentation)
                .collect(Collectors.toList());
    }

}
