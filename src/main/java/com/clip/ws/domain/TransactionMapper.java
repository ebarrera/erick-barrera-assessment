package com.clip.ws.domain;

import com.clip.ws.entity.Transaction;
import com.clip.ws.representation.TransactionRepresentation;
import com.clip.ws.representation.request.TransactionRequest;

public class TransactionMapper {

    private TransactionMapper() {
    }

    public static Transaction requestToTransaction(TransactionRequest transactionRequest) {
        return Transaction.builder()
                .amount(transactionRequest.getAmount())
                .description(transactionRequest.getDescription())
                .date(transactionRequest.getLocalDate())
                .build();
    }

    public static TransactionRepresentation toRepresentation(Transaction transaction) {
        return TransactionRepresentation.builder()
                .amount(transaction.getAmount())
                .description(transaction.getDescription())
                .userId(transaction.getUserId())
                .date(transaction.getDate())
                .transactionId(transaction.getId())
                .build();
    }

}
