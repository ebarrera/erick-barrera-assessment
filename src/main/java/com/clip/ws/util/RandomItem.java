package com.clip.ws.util;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.stream.IntStream;

public class RandomItem<T> {

    private final List<T> initialList;

    public RandomItem(List<T> list) {
        this.initialList = list;
    }

    public List<T> retrieve(int numberOfItems) {
        int seed = new Random().nextInt();
        return retrieve(seed, numberOfItems);
    }

    private void shuffle(List<T> list, int seed) {
        Random random = new Random(seed);
        Object[] array = list.toArray();

        for (int i = 0; i < array.length; i++) {
            int randomPosition = random.nextInt(array.length);
            Object temp = array[i];
            array[i] = array[randomPosition];
            array[randomPosition] = temp;
        }

        ListIterator it = list.listIterator();

        for (Object e : array) {
            it.next();
            it.set(e);
        }
    }

    private List<T> retrieve(int seed, int numberOfItems) {
        shuffle(initialList, seed);

        // Create new list with the number of item size
        List<T> newList = new ArrayList<>();

        IntStream.range(0, numberOfItems)
                .forEach(index -> newList.add(initialList.get(index)));

        return newList;
    }

}
