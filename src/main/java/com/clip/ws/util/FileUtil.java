package com.clip.ws.util;

import java.io.File;

public final class FileUtil {

    private FileUtil() {
    }

    public static void directoryAssurance(String directory) {
        File dir = new File(directory);

        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

}
