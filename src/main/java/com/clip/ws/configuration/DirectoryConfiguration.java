package com.clip.ws.configuration;

import com.clip.ws.util.FileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class DirectoryConfiguration {

    @Value("${service.system-file.parent}")
    private String parentDirectory;

    @Bean
    public File parentDirectory() {
        FileUtil.directoryAssurance(parentDirectory);
        return new File(parentDirectory);
    }

}
