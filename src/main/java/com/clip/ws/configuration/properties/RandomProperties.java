package com.clip.ws.configuration.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class RandomProperties {

    @Value("${service.transactions.random-max-subdirectories}")
    private int maxSubdirectories;

    @Value("${service.transactions.random-max-retrieve}")
    private int maxRetrieve;

}
