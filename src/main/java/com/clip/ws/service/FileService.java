package com.clip.ws.service;

import com.clip.ws.util.FileUtil;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class FileService {

    private final File parentDirectory;
    private final String JSON_EXTENSION = ".json";

    public FileService(File parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    public File parentDirectory() {
        return parentDirectory;
    }

    public String createDirectory(String directoryName) {
        String path = parentDirectory.getPath().concat(File.separator).concat(directoryName);
        FileUtil.directoryAssurance(path);

        return path;
    }

    public void writeJsonFile(@NonNull String directoryPath, @NonNull String fileName, String content) throws IOException {
        String fileNameWithExtension = fileName.concat(JSON_EXTENSION);
        String path = directoryPath.concat(File.separator).concat(fileNameWithExtension);

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(path))) {
            writer.write(content);
        }
    }

    public <T> File getSubDirectory(T subDirectory) {
        String subDirectoryPath = parentDirectory.getPath().concat(File.separator).concat(subDirectory.toString());
        FileUtil.directoryAssurance(subDirectoryPath);

        return new File(subDirectoryPath);
    }

}
