package com.clip.ws.service;

import com.clip.ws.entity.Transaction;
import com.clip.ws.repository.TransactionRepository;
import com.clip.ws.representation.request.TransactionRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.clip.ws.domain.TransactionMapper.requestToTransaction;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Transaction create(Long userId, TransactionRequest transactionRequest) {
        Transaction transaction = requestToTransaction(transactionRequest);
        return transactionRepository.addTransaction(userId, transaction);
    }

    public List<Transaction> getTransactions(Long userId) {
        return transactionRepository.getTransactions(userId);
    }

    public Optional<Transaction> getTransaction(UUID id, Long userId) {
        return transactionRepository.getTransaction(id, userId);
    }

    public BigDecimal sum(Long userId) {
        return transactionRepository.sumTransactions(userId);
    }

    public List<Transaction> random() {
        return transactionRepository.randomTransactions();
    }

}
