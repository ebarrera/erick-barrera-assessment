package com.clip.ws.repository.impl;

import com.clip.ws.configuration.properties.RandomProperties;
import com.clip.ws.entity.Transaction;
import com.clip.ws.exception.ResourceNotProcessableException;
import com.clip.ws.repository.TransactionRepository;
import com.clip.ws.service.FileService;
import com.clip.ws.util.JsonUtil;
import com.clip.ws.util.RandomItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Component
public class TransactionRepositoryImpl implements TransactionRepository {

    private final FileService fileService;
    private final RandomProperties randomProperties;

    public TransactionRepositoryImpl(FileService fileService, RandomProperties randomProperties) {
        this.fileService = fileService;
        this.randomProperties = randomProperties;
    }

    @Override
    @CacheEvict(value = "transactions", allEntries = true)
    public Transaction addTransaction(Long userId, Transaction transaction) {
        transaction = transaction.toBuilder()
                .id(UUID.randomUUID())
                .userId(userId)
                .build();

        String userDirectoryPath = fileService.createDirectory(userId.toString());
        String fileName = transaction.getId().toString();
        String content = JsonUtil.toJson(transaction);

        try {
            log.info("Creating transaction {}", transaction);
            fileService.writeJsonFile(userDirectoryPath, fileName, content);
        } catch (IOException e) {
            String message = String.format("Error creating a transaction [%s] in [%s]", transaction.getId(), userDirectoryPath);
            throw new ResourceNotProcessableException(message);
        }

        return transaction;
    }

    @Override
    @Cacheable(value = "transaction", key = "#id.toString().concat('|').concat(#userId)")
    public Optional<Transaction> getTransaction(UUID id, Long userId) {
        return getTransactions(userId).stream()
                .filter(transaction -> transaction.getId().equals(id))
                .findFirst();
    }

    @Override
    @Cacheable(value = "transactions", key = "#userId")
    public List<Transaction> getTransactions(Long userId) {
        File subDirectory = fileService.getSubDirectory(userId);

        if (subDirectory.listFiles() == null) {
            return Collections.emptyList();
        }

        return Arrays.stream(subDirectory.listFiles())
                .filter(File::exists)
                .map(file -> JsonUtil.fromFile(file, Transaction.class))
                .collect(Collectors.toList());
    }

    @Override
    public BigDecimal sumTransactions(Long userId) {
        List<Transaction> transactions = getTransactions(userId);

        return transactions.stream()
                .map(Transaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public List<Transaction> randomTransactions() {
        File parent = fileService.parentDirectory();
        File[] subDirectories = parent.listFiles();

        List<String> subDirectoriesName = new ArrayList<>();

        // Retrieve all subdirectories with at least one transaction
        assert subDirectories != null;
        Arrays.stream(subDirectories)
                .filter(subDirectory -> subDirectory.listFiles().length > 0)
                .limit(randomProperties.getMaxSubdirectories())
                .map(File::getPath)
                .forEach(subDirectoriesName::add);

        // Merging the subdirectories content
        List<Transaction> transactions = subDirectoriesName.stream()
                .map(File::new)
                .map(File::listFiles)
                .filter(Objects::nonNull)
                .flatMap(Arrays::stream)
                .map(file -> JsonUtil.fromFile(file, Transaction.class))
                .collect(Collectors.toList());

        RandomItem<Transaction> randomItem = new RandomItem<>(transactions);

        return randomItem.retrieve(randomProperties.getMaxRetrieve());
    }
}
