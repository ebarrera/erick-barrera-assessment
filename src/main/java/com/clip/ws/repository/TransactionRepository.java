package com.clip.ws.repository;

import com.clip.ws.entity.Transaction;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TransactionRepository {

    Transaction addTransaction(Long userId, Transaction transaction);

    Optional<Transaction> getTransaction(UUID id, Long userId);

    List<Transaction> getTransactions(Long userId);

    BigDecimal sumTransactions(Long userId);

    List<Transaction> randomTransactions();

}
