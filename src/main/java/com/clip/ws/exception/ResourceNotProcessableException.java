package com.clip.ws.exception;

public class ResourceNotProcessableException extends RuntimeException {

    public ResourceNotProcessableException() {
        super();
    }

    public ResourceNotProcessableException(String message) {
        super(message);
    }

}
