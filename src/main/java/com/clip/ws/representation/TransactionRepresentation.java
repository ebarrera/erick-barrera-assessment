package com.clip.ws.representation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class TransactionRepresentation {

    @JsonProperty("transaction_id")
    private UUID transactionId;

    @JsonProperty("user_id")
    private Long userId;

    private BigDecimal amount;
    private String description;
    private LocalDate date;

}
