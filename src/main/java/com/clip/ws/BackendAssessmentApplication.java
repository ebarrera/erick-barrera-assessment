package com.clip.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BackendAssessmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendAssessmentApplication.class, args);
    }

}
