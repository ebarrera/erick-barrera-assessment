# Backed assessment

#### A little beginning ####

>If at first you don't succeed, call it version 1.0 __~ Erick Barrera__

#### Author ####
* Erick Barrera - **ebarreral.isc@gmail.com**

### Technology stack ###

- Java 11
- Lombok
- Maven
- Spring
- Docker

### How to run the application? ###

No matter the way that you decide to launch the application, the application should be available on 
[localhost:5008](http://localhost:5008/)

**> With Intellij IDE**

_Note: Lombok plugin for Intellij IDE is required_

1. Import the project on Intellij IDE
2. Create new configuration on top-right button
3. Create new application and select the main class

**> With maven**

1. Install the dependencies with `mvn install`
2. Run the command `mvn spring-boot:run`

**> As jar**

1. Assembly the application with `mvn package`
2. Execute the jar `java -jar target/backend-assessment.jar`

>_Note: Java 11 is required_

**> With docker-compose**
1. Run the command `docker-compose up`

**> With docker**

1. Build the image with `docker build -t erbalo/backend-assessment:latest .`
2. Run the command `docker run -p 5008:5008 -it erbalo/backend-assessment`

Or pull the image 

1. Pull the docker image `docker pull erbalo/backend-assessment`
2. Run the command `docker run -p 5008:5008 -it erbalo/backend-assessment`

### Testing

**> With maven**

1. Execute `mvn test`

**> With docker**

1. When you execute the command `docker build -t erbalo/backend-assessment:latest .` internally it's 
calling `mvn package`, this lifecycle step also is doing the `mvn test` just look the console.

### Call the api

The API was designed as RESTful, so, you can test the application with the [swagger-ui](http://localhost:5008/swagger-ui.html)
or with the following CURLs

> Note: To beautify the output, you can use [jq](https://stedolan.github.io/jq/),
> remove the jq command if you prefer

**- List transactions by user**

```bash
curl -X GET "http://localhost:5008/transactions" \
     -H "accept: application/json" \
     -H "x-user-id: <USER_ID>" | jq
```

**- Get transaction by id and user**

```bash
curl -X GET "http://localhost:5008/transactions/<TRANSACTION_ID>" \
     -H "accept: application/json" \
     -H "x-user-id: <USER_ID>" | jq
```

**- Get the sum for all transaction by user**

```bash
curl -X GET "http://localhost:5008/transactions/sum" \
     -H "accept: application/json" \
     -H "x-user-id: <USER_ID>" | jq
```

**- Create a transaction for specific user**

```bash
curl -X POST "http://localhost:5008/transactions" \
     -H "accept: application/json" \
     -H "Content-Type: application/json" \
     -H "x-user-id: <USER_ID>" \
     -d "{\"amount\": -1, \"description\": \"Some transaction\", \"local_date\": \"2019-10-15\"}" | jq
```

**- Get random transaction**

```bash
curl -X GET "http://localhost:5008/transactions/random" \
     -H "accept: application/json" | jq
```

### Considerations

If you run the application as docker, the system file (transactions folder) will be available inside the container.

To enter to the current container and navigate in the transactions folder:

```bash
docker exec -it <container_name_or_id> /bin/bash

Example:

docker exec -it erbalo_backend_api /bin/bash
```